<?php
/**
 * class-groups-restrict-categories-taxonomy-admin.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-restrict-categories
 * @since 1.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Taxonomy admin handlers. Adds access restriction sections to admin screens
 * for taxonomies.
*/
class Groups_Restrict_Categories_Taxonomy_Admin {

	/**
	 * Field name.
	 * @var string
	 */
	const GROUPS_READ = 'groups-read';

	/**
	 * Groups column header id.
	 * @var string
	 */
	const GROUPS = 'groups-read';

	/**
	 * Sets up the init action.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'wp_init' ), apply_filters( 'groups_restrict_categories_init_priority', Groups_Restrict_Categories::INIT_PRIORITY ) );
	}

	/**
	 * Registers our actions.
	 *
	 * We assume that anyone who can edit a taxonomy, should also be allowed
	 * to apply groups/capability restrictions. But only group admins are
	 * allowed to quick-create new group-capability pairs.
	 */
	public static function wp_init() {
		$taxonomies = Groups_Restrict_Categories::get_taxonomies( 'names' );
		foreach( $taxonomies as $taxonomy ) {
			// Render restriction options when adding a new taxonomy term.
			add_action( "${taxonomy}_add_form_fields", array( __CLASS__, 'taxonomy_add_form_fields' ) );

			// Render restriction options when editing a taxonomy term.
			add_action( "${taxonomy}_edit_form", array( __CLASS__, 'taxonomy_edit_form' ), 10, 2 );

			// Save restrictions for a new taxonomy term.
			add_action( "created_${taxonomy}", array( __CLASS__, 'created_taxonomy' ), 10, 2 );

			// Save restrictions for a taxonomy term.
			add_action( "edited_${taxonomy}", array( __CLASS__, 'edited_taxonomy' ), 10, 2 );

			// Remove restrictions when a taxonomy term is deleted.
			add_action( "delete_${taxonomy}", array( __CLASS__, 'delete_taxonomy' ), 10, 3 );

			// Remove restrictions when a group is deleted.
			add_action( 'groups_deleted_group', array( __CLASS__, 'groups_deleted_group' ) );

			// sortable groups read access restriction column - see the get_terms_args filter below which is added in general to handle these
			// $_sortable = apply_filters( "manage_{$this->screen->id}_sortable_columns", $this->get_sortable_columns() );
			add_filter( "manage_edit-${taxonomy}_sortable_columns", array( __CLASS__, 'manage_edit_taxonomy_sortable_columns' ) );

			// Add the Access Restriction column in the taxonomy overview.
			add_filter( "manage_edit-${taxonomy}_columns", array( __CLASS__, 'manage_edit_taxonomy_columns' ) );

			// Render the access restriction capabilities.
			// return apply_filters( "manage_{$this->screen->taxonomy}_custom_column", '', $column_name, $tag->term_id );
			add_filter( "manage_${taxonomy}_custom_column", array( __CLASS__, 'manage_taxonomy_custom_column' ), 10, 3 );
		}

		// handle our sortable column
		add_filter( 'get_terms_args', array( __CLASS__, 'get_terms_args' ), 10, 2 );

		// scripts
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'admin_enqueue_scripts' ) );
	}

	/**
	 * Enqueue the select script where we need it.
	 *
	 * @param string $hook
	 */
	public static function admin_enqueue_scripts( $hook ) {
		switch( $hook ) {
			case 'term.php' :
			case 'edit-tags.php' :
				Groups_UIE::enqueue( 'select' );
				break;
		}
	}

	/**
	 * Restrictions rendered before the "Add New (Taxonomy)" button.
	 *
	 * @param string $taxonomy
	 */
	public static function taxonomy_add_form_fields( $taxonomy ) {
		self::panel( $taxonomy );
	}

	/**
	 * Hook in wp-admin/edit-tag-form.php - add restrictions.
	 *
	 * @param string $tag
	 * @param string $taxonomy
	 */
	public static function taxonomy_edit_form( $tag, $taxonomy ) {
		if ( isset( $tag->term_id ) && $tag->term_id == get_option( 'default_category' ) ) {
			// don't allow to set a restriction on the default category
		} else {
			self::panel( $tag, $taxonomy );
		}
	}

	/**
	 * Renders our restriction panel.
	 */
	public static function panel( $tag = null ) {

		global $post, $wpdb;

		$output        = '';
		$term_id       = isset( $tag->term_id ) ? $tag->term_id : null;
		$taxonomy_name = isset( $tag->taxonomy ) ? $tag->taxonomy : $tag;
		$taxonomy      = get_taxonomy( $taxonomy_name );

		$singular_name = __( 'Taxonomy', GRC_PLUGIN_DOMAIN );
		if ( $taxonomy !== null ) {
			$labels = isset( $taxonomy->labels ) ? $taxonomy->labels : null;
			if ( $labels !== null ) {
				if ( isset( $labels->singular_name ) )  {
					$singular_name = __( $labels->singular_name );
				}
			}
		}

		// note that no specific nonce is needed, the taxonomy form already has one

		$output .= '<div class="form-field">';
		if ( Groups_Access_Meta_Boxes::user_can_restrict() ) {

			$user = new Groups_User( get_current_user_id() );
			if ( method_exists( 'Groups_Access_Meta_Boxes', 'get_user_can_restrict_group_ids' ) ) {
				$include = Groups_Access_Meta_Boxes::get_user_can_restrict_group_ids();
			} else {
				$include = $user->group_ids_deep;
			}
			$groups = Groups_Group::get_groups( array( 'order_by' => 'name', 'order' => 'ASC', 'include' => $include ) );
			// get access restriction groups for $term_id
			if ( $term_id !== null ) {
				// existing term
				$groups_read = Groups_Restrict_Categories::get_term_read_groups( $term_id );
			} else {
				// new term
				$groups_read = array();
			}

			$read_help = sprintf(
				__( 'You can restrict the visibility of this %1$s to group members. Choose one or more groups that are allowed to read this %2$s. If no groups are chosen, the %3$s is visible to anyone.', GROUPS_PLUGIN_DOMAIN ),
				$singular_name,
				$singular_name,
				$singular_name
			);
			if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
				$read_help .= ' ' . __( 'You can create a new group by indicating the group\'s name.', GROUPS_PLUGIN_DOMAIN );
			}

			$output .= sprintf(
				'<label title="%s">',
				esc_attr( $read_help )
			);
			$output .= __( 'Read', GROUPS_PLUGIN_DOMAIN );
			$output .= ' ';

			$output .= sprintf(
				'<select class="select groups-read" name="%s" multiple="multiple" placeholder="%s" data-placeholder="%s" title="%s">',
				self::GROUPS_READ . '[]',
				esc_attr( __( 'Anyone &hellip;', GROUPS_PLUGIN_DOMAIN ) ),
				esc_attr( __( 'Anyone &hellip;', GROUPS_PLUGIN_DOMAIN ) ),
				esc_attr( $read_help )
			);
			$output .= '<option value=""></option>';
			foreach( $groups as $group ) {
				$output .= sprintf( '<option value="%s" %s>', esc_attr( $group->group_id ), is_array( $groups_read ) && in_array( $group->group_id, $groups_read ) ? ' selected="selected" ' : '' );
				$output .= wp_filter_nohtml_kses( $group->name );
				$output .= '</option>';
			}
			$output .= '</select>';
			$output .= '</label>';
			$output .= Groups_UIE::render_select(
				'.select.groups-read',
				true,
				true,
				current_user_can( GROUPS_ADMINISTER_GROUPS )
			);
			$output .= '<p class="description">';
			$output .= sprintf(
				__( 'Restricts the visibility of this %s to members of the chosen groups.', GROUPS_PLUGIN_DOMAIN ),
				$singular_name
			);
			$output .= '</p>';
		} else {
			$output .= '<p class="description" style="width:92%;padding:0 8px;">';
			$output .= __( 'You cannot set any access restrictions.', GRC_PLUGIN_DOMAIN );
			$style = 'cursor:help;vertical-align:middle;';
			if ( current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
				$style = 'cursor:pointer;vertical-align:middle;';
				$output .= sprintf( '<a href="%s">', esc_url( admin_url( 'admin.php?page=groups-admin-options' ) ) );
			}
			$output .= sprintf( '<img style="%s" alt="?" title="%s" src="%s" />', $style, esc_attr( __( 'You must be in a group that has at least one capability enabled to enforce read access.', GRC_PLUGIN_DOMAIN ) ), esc_attr( GROUPS_PLUGIN_URL . 'images/help.png' ) );
			if ( current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
				$output .= '</a>';
			}
			$output .= '</p>';
		}

		$output .= '</div>'; // .form-field
		echo $output;

	}

	/**
	 * Save groups for a new taxonomy term.
	 *
	 * @param int $term_id
	 * @param int $tt_id
	 */
	public static function created_taxonomy( $term_id, $tt_id ) {
		self::edited_taxonomy( $term_id, $tt_id );
	}

	/**
	 * Save taxonomy term access restriction groups.
	 *
	 * @param int $term_id term ID
	 * @param int $tt_id taxonomy ID
	 */
	public static function edited_taxonomy( $term_id, $tt_id ) {
		if (
			( $term_id != get_option( 'default_category' ) ) &&
			Groups_Access_Meta_Boxes::user_can_restrict()
		) {
			$user = new Groups_User( get_current_user_id() );
			if ( method_exists( 'Groups_Access_Meta_Boxes', 'get_user_can_restrict_group_ids' ) ) {
				$include = Groups_Access_Meta_Boxes::get_user_can_restrict_group_ids();
			} else {
				$include = $user->group_ids_deep;
			}
			$groups = Groups_Group::get_groups( array( 'order_by' => 'name', 'order' => 'ASC', 'include' => $include ) );
			$user_group_ids_deep = array();
			foreach( $groups as $group ) {
				$user_group_ids_deep[] = $group->group_id;
			}
			$group_ids = array();
			$submitted_group_ids = !empty( $_POST[self::GROUPS_READ] ) && is_array( $_POST[self::GROUPS_READ] ) ? $_POST[self::GROUPS_READ] : array();

			// assign requested groups and create and assign new groups if allowed
			foreach( $submitted_group_ids as $group_id ) {
				if ( is_numeric( $group_id ) ) {
					if ( is_array( $user_group_ids_deep ) && in_array( $group_id, $user_group_ids_deep ) ) {
						$group_ids[] = $group_id;
					}
				} else {
					if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
						$creator_id = get_current_user_id();
						$datetime   = date( 'Y-m-d H:i:s', time() );
						$name       = ucwords( strtolower( trim( preg_replace( '/\s+/', ' ', $group_id ) ) ) );
						if ( strlen( $name ) > 0 ) {
							if ( !( $group = Groups_Group::read_by_name( $name ) ) ) {
								if ( $group_id = Groups_Group::create( compact( 'creator_id', 'datetime', 'name' ) ) ) {
									if ( Groups_User_Group::create( array( 'user_id' => $creator_id, 'group_id' => $group_id ) ) ) {
										$group_ids[] = $group_id;
									}
								}
							}
						}
					}
				}
			}
			Groups_Restrict_Categories::set_term_read_groups( $term_id, $group_ids );
		}
	}

	/**
	 * Remove restrictions for a taxonomy term.
	 *
	 * @param int $term_id
	 * @param int $tt_id taxonomy
	 * @param object $deleted_term
	 */
	public static function delete_taxonomy( $term_id, $tt_id, $deleted_term ) {
		Groups_Restrict_Categories::set_term_read_groups( $term_id, array() );
	}

	/**
	 * Remove the deleted group if used for restrictions related to a taxonomy term.
	 * @param int $group_id ID of the deleted group
	 */
	public static function groups_deleted_group( $group_id ) {
		$taxonomies = Groups_Restrict_Categories::get_taxonomies( 'names' );
		$term_ids = get_terms( $taxonomies, array( 'fields' => 'ids', 'hide_empty' => false ) );
		if ( is_array( $term_ids ) ) {
			$delete_groups = array( $group_id );
			foreach( $term_ids as $term_id ) {
				Groups_Restrict_Categories::delete_term_read_groups( $term_id, $delete_groups );
			}
		}
	}

	/**
	 * Adds the Groups column.
	 * @param array $columns
	 * @return array
	 */
	public static function manage_edit_taxonomy_columns( $columns ) {
		$columns[self::GROUPS] = __( 'Groups', GRC_PLUGIN_DOMAIN );
		return $columns;
	}

	/**
	 * Render groups for a taxonomy term.
	 * @param string $content
	 * @param string $column_name
	 * @param int $term_id
	 * @return string
	 */
	public static function manage_taxonomy_custom_column( $content, $column_name, $term_id ) {
		if ( $column_name == self::GROUPS ) {
			$content .= self::get_groups_read_html( $term_id );
		}
		return $content;
	}

	/**
	 * Make the groups column sortable.
	 *
	 * @param array $columns
	 */
	public static function manage_edit_taxonomy_sortable_columns( $sortable_columns ) {
		if ( is_array( $sortable_columns ) && !key_exists( self::GROUPS, $sortable_columns ) ) {
			$sortable_columns[self::GROUPS] = self::GROUPS;
		}
		return $sortable_columns;
	}

	/**
	 * Recognize a request to sort by group and modify the query args to handle it.
	 *
	 * @param array $args term query args
	 * @param array $taxonomies what taxonomies
	 */
	public static function get_terms_args( $args, $taxonomies ) {
		if ( is_admin() ) {
			// only act on taxonomy screens
			include_once ABSPATH . 'wp-admin/includes/screen.php';
			if ( function_exists( 'get_current_screen' ) ) {
				$screen = get_current_screen();
				if (
					!empty( $screen ) &&
					!empty( $screen->id ) &&
					is_array( $taxonomies ) && in_array( str_replace( 'edit-', '', $screen->id ), $taxonomies )
				) {
					// check if the table should be sorted by related groups
					if (
						!empty( $args ) &&
						isset( $args['orderby'] ) &&
						( self::GROUPS == $args['orderby'] )
					) {
						$args['orderby'] = 'meta_value';
						// For the record, don't do this:
						//$args['meta_key'] = self::GROUPS_READ;
						// ... otherwise it will result in a query with an AND requiring a value for the meta key
						// and it will only show entries for terms that have a group related and the
						// rest would be omitted.
						// To get all entries, whether they have or not any meta values related, we have to
						// construct this OR'd query so it retrieves results that have and results that don't have
						// any groups related:
						$meta_query = array(
							'relation' => 'OR',
							array(
								'key' => self::GROUPS_READ,
								'value' => '',
								'compare' => 'NOT EXISTS'
							),
							array(
								'key' => self::GROUPS_READ,
								'value' => array(),
								'compare' => 'NOT IN'
							)
						);
						// don't overwrite an existing meta_query
						if ( !empty( $args['meta_query'] ) ) {
							$meta_query = array(
								'relation' => 'AND',
								$args['meta_query'],
								$meta_query
							);
						}
						$args['meta_query'] = $meta_query;
					}
				}
			}
		}
		return $args;
	}

	/**
	 * Add a taxonomy column to the post type overview.
	 *
	 * @todo we should be able to show the taxonomy terms that protect a post
	 *
	 * @param array $posts_columns
	 * @return array
	 */
// 	public static function manage_??????????_posts_columns( $posts_columns ) {
// 		$posts_columns['??????????'] = __( '?????????? Taxonomy', GRC_PLUGIN_DOMAIN );
// 		return $posts_columns;
// 	}

	/**
	 * Render taxonomy terms in the post type overview.
	 * @param string $column_name
	 * @param int $post_id
	 */
// 	public static function manage_??????????_posts_custom_column( $column_name, $post_id ) {
// 		if ( $column_name == '??????????' ) {
// 			$terms = wp_get_post_terms( $post_id, '??????????' );
// 			foreach( $terms as $term ) {
// 				$args = array();
// 				$args['action'] = 'edit';
// 				$args['tag_ID'] = $term->term_id;
// 				$args['post_type'] = 'xxxxxxxxxx';
// 				$args['taxonomy'] = '??????????';
// 				echo sprintf( '<a href="%s">%s</a>',
// 					esc_url( add_query_arg( $args, 'edit-tags.php' ) ),
// 					esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, '??????????', 'display' ) )
// 				);
// 			}
// 		}
// 	}

	/**
	 * Renders the list of groups restricting read access for the given taxonomy term.
	 *
	 * @param int $term_id
	 * @return string HTML
	 */
	private static function get_groups_read_html( $term_id ) {
		$output = '';
		$groups_read = Groups_Restrict_Categories::get_term_read_groups( $term_id );
		if ( !empty( $groups_read ) ) {
			$groups = Groups_Group::get_groups( array( 'order_by' => 'name', 'order' => 'ASC', 'include' => $groups_read ) );
			$output .= '<ul style="margin:0">';
			foreach( $groups as $group ) {
				$output .= '<li>';
				$output .= wp_filter_nohtml_kses( $group->name );
				$output .= '</li>';
			}
			$output .= '</ul>';
		}
		return $output;
	}

}
Groups_Restrict_Categories_Taxonomy_Admin::init();
