<?php
/**
 * class-groups-restrict-categories.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-restrict-categories
 * @since 1.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin core class.
 */
class Groups_Restrict_Categories {

	const OPTIONS       = 'grc_options';
	const INIT_PRIORITY = 999;

	const CACHE_GROUP         = 'groups';
	const RESTRICTED_TERM_IDS = 'restricted_term_ids';
	const CONTROLS_TERM       = 'controls_term';
	const USER_CAN_READ       = 'user_can_read';

	const CAN_READ_TERM       = 'can_read_term';
	const TERMMETA_PREFIX     = 'groups-';
	const READ                = 'read';

	private static $taxonomies = array();

	/**
	 * Adds actions/filters.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'wp_init' ), apply_filters( 'groups_restrict_categories_init_priority', self::INIT_PRIORITY ) );

		// WooCommerce:

		// Don't allow to add products restricted by terms to the cart.
		// This is necessary to avoid products being added to the cart
		// through a direct link http://example.com/?post_type=product&add-to-cart=123
		// This filter must be added earlier than in our wp_init to take effect.
		add_filter( 'woocommerce_is_purchasable', array( __CLASS__, 'woocommerce_is_purchasable' ), 10, 2 );
	}

	/**
	 * Retrieves the post types option holding the write and read comments
	 * group requirements.
	 */
	public static function wp_init() {

		// Taxonomy filters:

		// Control access to taxonomy term admin pages and actions.
		// We can't use that (comes in too late and with different semantics and context).
		// See note on method below.
		//add_filter( 'get_term', array( __CLASS__, 'get_term' ), 10, 2 );
		// Instead, we deny access to the page:
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );

		// Control terms requested through get_terms().
		add_filter( 'list_terms_exclusions', array( __CLASS__, 'list_terms_exclusions' ), 10, 3 );

		// Post filters:

		// Exclude posts related to restricted taxonomy terms.
		add_filter( 'posts_where', array( __CLASS__, 'posts_where' ), 10, 2 );

		// Page taxonomy access restrictions (for possible custom taxonomies).
		add_filter( 'get_pages', array( __CLASS__, 'get_pages' ) );

		// Post taxonomy access restrictions.
		if ( apply_filters( 'groups_restrict_categories_filter_the_posts', false ) ) {
			add_filter( 'the_posts', array( __CLASS__, 'the_posts' ), 1, 2 );
		}

		// Filter menu
		add_filter( 'wp_get_nav_menu_items', array( __CLASS__, 'wp_get_nav_menu_items' ), 1, 3 );

		// Filter excerpts.
		add_filter( 'get_the_excerpt', array( __CLASS__, 'get_the_excerpt' ) );

		// Filter contents.
		add_filter( 'the_content', array( __CLASS__, 'the_content' ) );

		// Controls permission to edit or delete posts.
		add_filter( 'map_meta_cap', array( __CLASS__, 'map_meta_cap' ), 10, 4 );

		// filter comment feed
		add_filter( 'comment_feed_where', array( __CLASS__, 'comment_feed_where' ), 10, 2 );

		// filter comments
		add_filter( 'comments_clauses', array( __CLASS__, 'comments_clauses' ), 10, 2 );

		// filter comment counts
		add_filter( 'groups_comment_access_comment_count_where', array( __CLASS__, 'groups_comment_access_comment_count_where'), 10, 2 );
	}

	/**
	 * Not used but left for reference.
	 * The filter doesn't work the way that we could use to
	 * restrict access to the term. For example, edit-tags.php would still
	 * show up although with empty fields.
	 *
	 * Quoting from get_term() :
	 * "... 'get_term' hook ... Must return term object. ..."
	 *
	 * @param object $term
	 * @param string $taxonomy
	 * @return object modified $term 
	 */
	// 	public static function get_term( $term, $taxonomy ) {
	// 		if ( isset( $term->term_id ) ) {
	// 			if ( !self::user_can_read_term(get_current_user_id(), $term->term_id ) ) {
	// 				$term = (object) array(
	// 					'term_id' => null,
	// 					'name' => null,
	// 					'taxonomy' => null,
	// 					'parent' => null,
	// 					'description' => null
	// 				);
	// 			}
	// 		}
	// 		return $term;
	// 	}

	/**
	 * Restricts access to taxonomy term pages and actions on the admin side,
	 * through edit-tags.php.
	 */
	public static function admin_init() {

		global $pagenow;

		$user_id = get_current_user_id();

		// admin override or Groups admin
		if (
			function_exists( '_groups_admin_override' ) && _groups_admin_override() ||
			current_user_can( GROUPS_ADMINISTER_GROUPS )
		) {
			return;
		}

		switch( $pagenow ) {
			case 'edit-tags.php' :
				if ( isset( $_REQUEST['tag_ID'] ) ) {
					$term_id = (int) $_REQUEST['tag_ID'];
					if ( !self::user_can_read_term( get_current_user_id(), $term_id ) ) {
						wp_die( 'Access denied.', GRC_PLUGIN_DOMAIN );
					}
				}
				break;
		}
	}

	/**
	 * Returns a list of group IDs that grant read access to the term.
	 *
	 * @param int $term_id
	 * @return array of int, group IDs
	 */
	public static function get_read_group_ids( $term_id ) {
		return get_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ );
	}

	/**
	 * Returns true if the user can access the term.
	 *
	 * @param int $user_id
	 * @param int $term_id
	 * @return boolean
	 */
	public static function user_can_read_term( $user_id, $term_id ) {
		$result = false;
		if ( self::controls_term( $term_id ) ) {

			if ( $user_id === null ) {
				$user_id = get_current_user_id();
			}

			$cached = Groups_Cache::get( self::CAN_READ_TERM . '_' . $user_id . '_' . $term_id, self::CACHE_GROUP );

			if ( $cached !== null ) {
				$result = $cached->value;
				unset( $cached );
			} else {
				if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ||
					current_user_can( GROUPS_ADMINISTER_GROUPS )
				) {
					$result = true;
				} else {
					$groups_user = new Groups_User( $user_id );
					$group_ids   = self::get_read_group_ids( $term_id );
					if ( empty( $group_ids ) ) {
						$result = true;
					} else {
						$ids = array_intersect( $groups_user->group_ids_deep, $group_ids );
						$result = !empty( $ids );
					}
				}
				$result = apply_filters( 'groups_restrict_categories_user_can_read_term', $result, $term_id, $user_id );
				Groups_Cache::set( self::CAN_READ_TERM . '_' . $user_id . '_' . $term_id, $result, self::CACHE_GROUP );
			}
		}
		return $result;
	}

	/**
	 * Returns all term IDs that the user is not allowed to read.
	 *
	 * @deprecated
	 *
	 * @param int $user_id
	 * @return array of int with term IDs
	 */
	public static function old_get_user_restricted_term_ids( $user_id ) {
		$cached = class_exists( 'Groups_Cache' ) ? Groups_Cache::get( self::RESTRICTED_TERM_IDS . intval( $user_id ), self::CACHE_GROUP ) : null;
		if ( $cached !== null ) {
			$restricted_term_ids = $cached->value;
			unset( $cached );
		} else {
			$restricted_term_ids = array();
			if (
				function_exists( '_groups_admin_override' ) && _groups_admin_override() ||
				current_user_can( GROUPS_ADMINISTER_GROUPS )
			) {
				// no restrictions with admin override enabled or for Groups admin
			} else {
				$taxonomies = self::get_controlled_taxonomies();
				// Temporarily disable the filter so that we can retrieve all terms
				// for the current user including those that are restricted.
				remove_filter( 'list_terms_exclusions', array( __CLASS__, 'list_terms_exclusions' ), 10 );
				$term_ids = get_terms( $taxonomies, array( 'fields' => 'ids', 'hide_empty' => false ) );
				add_filter( 'list_terms_exclusions', array( __CLASS__, 'list_terms_exclusions' ), 10, 3 );
				if ( is_array( $term_ids ) ) {
					$groups_user = new Groups_User( $user_id );
					foreach( $term_ids as $term_id ) {
						$read_group_ids = self::get_term_read_groups( $term_id );
						if ( !empty( $read_group_ids ) ) {
							$restricted = true;
							foreach( $read_group_ids as $read_group_id ) {
								if ( $groups_user->is_member( $read_group_id ) ) {
									$restricted = false;
									break;
								}
							}
							if ( $restricted ) {
								$restricted_term_ids[] = $term_id;
							}
						}
					}
				}
				$restricted_term_ids = array_map( 'intval', $restricted_term_ids );
			}
			if ( class_exists( 'Groups_Cache' ) ) {
				Groups_Cache::set( self::RESTRICTED_TERM_IDS . intval( $user_id ), $restricted_term_ids, self::CACHE_GROUP );
			}
		}
		return $restricted_term_ids;
	}

	/**
	 * Returns all term IDs that the user is not allowed to read.
	 *
	 * @param int $user_id
	 * @return array of int with term IDs
	 */
	public static function get_user_restricted_term_ids( $user_id ) {

		global $wpdb;

		$cached = class_exists( 'Groups_Cache' ) ? Groups_Cache::get( self::RESTRICTED_TERM_IDS . intval( $user_id ), self::CACHE_GROUP ) : null;
		if ( $cached !== null ) {
			$restricted_term_ids = $cached->value;
			unset( $cached );
		} else {
			$restricted_term_ids = array();
			if (
				function_exists( '_groups_admin_override' ) && _groups_admin_override() ||
				current_user_can( GROUPS_ADMINISTER_GROUPS )
			) {
				// no restrictions with admin override enabled or for Groups admin
			} else {

				// 1. Get all the groups that the user belongs to, including those that are inherited:
				$group_ids = array();
				if ( $user = new Groups_User( $user_id ) ) {
					$group_ids_deep = $user->group_ids_deep;
					if ( is_array( $group_ids_deep ) ) {
						$group_ids = $group_ids_deep;
					}
				}

				if ( count( $group_ids ) > 0 ) {
					$group_ids = "'" . implode( "','", $group_ids ) . "'";
				} else {
					$group_ids = '\'\'';
				}

				$taxonomies = self::get_controlled_taxonomies();
				if ( count( $taxonomies ) == 0 ) {
					return $where;
				}
				$taxonomies_in = "'" . implode( "','", array_map( 'esc_sql', $taxonomies ) ) . "'";

				$terms = $wpdb->get_results( sprintf(
					"SELECT DISTINCT tt.term_id FROM $wpdb->term_taxonomy tt ".
					"LEFT JOIN $wpdb->termmeta tm ON tt.term_id = tm.term_id " .
					"WHERE tt.taxonomy IN (%s) AND " .
					"tm.meta_key = '%s' AND tm.meta_value NOT IN (%s) AND " . // terms restricted to groups that the user is not a member of ...
					"tt.term_id NOT IN ( " . // ... MINUS ...
					"SELECT tt.term_id FROM $wpdb->term_taxonomy tt ".
					"LEFT JOIN $wpdb->termmeta tm ON tt.term_id = tm.term_id " .
					"WHERE tt.taxonomy IN (%s) AND " .
					"tm.meta_key = '%s' AND tm.meta_value IN (%s)" . // ... terms restricted to groups that the user IS a member of
					") ",
					$taxonomies_in,
					self::TERMMETA_PREFIX . self::READ,
					$group_ids,
					$taxonomies_in,
					self::TERMMETA_PREFIX . self::READ,
					$group_ids
				) );
				if ( is_array( $terms ) ) {
					foreach( $terms as $term ) {
						$restricted_term_ids[] = $term->term_id;
					}
					$restricted_term_ids = array_map( 'intval', $restricted_term_ids );
				}
			}
			if ( class_exists( 'Groups_Cache' ) ) {
				Groups_Cache::set( self::RESTRICTED_TERM_IDS . intval( $user_id ), $restricted_term_ids, self::CACHE_GROUP );
			}
		}
		return $restricted_term_ids;
	}

	/**
	 * Filters out terms that are restricted.
	 *
	 * @param string $exclusions
	 * @param array $args
	 * @param array $taxonomies
	 * @return string $exclusions with appended term ID restrictions
	 */
	public static function list_terms_exclusions( $exclusions, $args, $taxonomies ) {

		if ( !apply_filters( 'groups_restrict_categories_list_terms_exclusions_apply', true, $exclusions, $args, $taxonomies ) ) {
			return $exclusions;
		}

		$user_id = get_current_user_id();

		// admin override ?
		if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ) {
			return $exclusions;
		}

		if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			return $exclusions;
		}

		$restricted_term_ids = self::get_user_restricted_term_ids( $user_id );
		if ( !empty( $restricted_term_ids ) ) {
			$restricted_term_ids = array_map( 'intval', $restricted_term_ids );
			$restricted_term_ids = implode( ',', $restricted_term_ids );
			$exclusions .= ' AND t.term_id NOT IN (' . $restricted_term_ids . ')';
		}

		return $exclusions;
	}

	/**
	 * Filters out posts that the user should not be able to access, based
	 * on taxonomy terms with access restrictions.
	 *
	 * @param string $where current where conditions
	 * @param WP_Query $query current query
	 * @return string modified $where
	 */
	public static function posts_where( $where, $query ) {

		global $wpdb;

		if ( !apply_filters( 'groups_restrict_categories_posts_where_apply', true, $where, $query ) ) {
			return $where;
		}

		if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ) {
			return $where;
		}

		if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			return $where;
		}

		$user_id = get_current_user_id();

		// For the record ... the approach below is similar to Groups_Post_Access::posts_where() but with our
		// optimized get_user_restricted_term_ids() it seems to be slightly faster than with the direct query.
		// Left for reference - note that the same thing would apply to our comments_clauses() :
		// 1. Get all the groups that the user belongs to, including those that are inherited:
		//$group_ids = array();
		//if ( $user = new Groups_User( $user_id ) ) {
		//	$group_ids_deep = $user->group_ids_deep;
		//	if ( is_array( $group_ids_deep ) ) {
		//		$group_ids = $group_ids_deep;
		//	}
		//}

		//if ( count( $group_ids ) > 0 ) {
		//	$group_ids = "'" . implode( "','", $group_ids ) . "'";
		//} else {
		//	$group_ids = '\'\'';
		//}

		//$taxonomies = self::get_controlled_taxonomies();
		//if ( count( $taxonomies ) == 0 ) {
		//	return $where;
		//}
		//$taxonomies_in = "'" . implode( "','", array_map( 'esc_sql', $taxonomies ) ) . "'";

		//$where .= sprintf(
		//	" AND {$wpdb->posts}.ID NOT IN ( " .
		//		"SELECT tr.object_id FROM $wpdb->term_relationships tr WHERE tr.term_taxonomy_id IN ( " .
		//			"SELECT tt.term_taxonomy_id FROM $wpdb->term_taxonomy tt ".
		//				"LEFT JOIN $wpdb->termmeta tm ON tt.term_id = tm.term_id " .
		//				"WHERE tt.taxonomy IN (%s) AND " .
		//				"tm.meta_key = '%s' AND tm.meta_value NOT IN (%s) AND " . // terms restricted to groups that the user is not a member of ...
		//				"tt.term_id NOT IN ( " . // ... MINUS ...
		//					"SELECT tt.term_id FROM $wpdb->term_taxonomy tt ".
		//						"LEFT JOIN $wpdb->termmeta tm ON tt.term_id = tm.term_id " .
		//						"WHERE tt.taxonomy IN (%s) AND " .
		//						"tm.meta_key = '%s' AND tm.meta_value IN (%s)" . // ... terms restricted to groups that the user IS a member of
		//				") " .
		//			") " .
		//		") ",
		//	$taxonomies_in,
		//	self::TERMMETA_PREFIX . self::READ,
		//	$group_ids,
		//	$taxonomies_in,
		//	self::TERMMETA_PREFIX . self::READ,
		//	$group_ids
		//);

		// $restricted_term_ids are terms that the current user is not allowed
		// to access. Any post that belongs to one of those terms should be
		// filtered out.
		// The resulting query should result in getting all posts that are
		// not in ANY of the restricted categories.
		$restricted_term_ids = self::get_user_restricted_term_ids( $user_id );
		if ( !empty( $restricted_term_ids ) && count( $restricted_term_ids) > 0) {
			$restricted_term_ids= "'" . implode( "','", array_map( 'esc_sql', $restricted_term_ids) ) . "'";
			$where .= " AND {$wpdb->posts}.ID NOT IN ";
			$where .= " ( ";
			$where .= sprintf(
				"SELECT object_id FROM $wpdb->term_relationships " .
				"LEFT JOIN $wpdb->term_taxonomy ON {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->term_taxonomy}.term_taxonomy_id ".
				"WHERE term_id IN (%s) ",
				$restricted_term_ids
			);
			$where .= " ) ";
		}

		return $where;
	}

	/**
	 * Filter feed comments.
	 *
	 * @param string $where
	 * @param WP_Query $query
	 * @return string
	 */
	public static function comment_feed_where( $where, $query ) {

		if ( !apply_filters( 'groups_restrict_categories_comment_feed_where_apply', true, $where, $query ) ) {
			return $where;
		}

		if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ) {
			return $where;
		}

		if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			return $where;
		}

		$pieces['where'] = $where;
		$pieces = self::comments_clauses( $pieces, null );
		return $pieces['where'];
	}

	/**
	 * Filter the comments based on term access restrictions.
	 *
	 * @param array $pieces
	 * @param WP_Comment_Query $comment_query
	 * @return array
	 */
	public static function comments_clauses( $pieces, $comment_query ) {

		global $wpdb;

		if ( !apply_filters( 'groups_restrict_categories_comments_clauses_apply', true, $pieces, $comment_query ) ) {
			return $pieces;
		}

		if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ) {
			return $pieces;
		}

		if ( current_user_can( GROUPS_ADMINISTER_GROUPS ) ) {
			return $pieces;
		}

		$user_id = get_current_user_id();
		$where = isset( $pieces['where'] ) ? $pieces['where'] : '';
		$restricted_term_ids = self::get_user_restricted_term_ids( $user_id );
		if ( !empty( $restricted_term_ids ) && count( $restricted_term_ids) > 0) {
			$restricted_term_ids= "'" . implode( "','", array_map( 'esc_sql', $restricted_term_ids) ) . "'";
			$where .= " AND {$wpdb->comments}.comment_post_ID NOT IN ";
			$where .= " ( ";
			$where .= sprintf(
				"SELECT object_id FROM $wpdb->term_relationships " .
					"LEFT JOIN $wpdb->term_taxonomy ON {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->term_taxonomy}.term_taxonomy_id ".
					"WHERE term_id IN (%s) ",
					$restricted_term_ids
			);
			$where .= " ) ";
		}
		$pieces['where'] = $where;
		return $pieces;
	}

	/**
	 * Add additional where conditions to filter comments per restricted terms.
	 *
	 * @param string $where
	 * @param int $post_id
	 * @return string
	 */
	public static function groups_comment_access_comment_count_where( $where, $post_id ) {
		global $wpdb;
		$user_id = get_current_user_id();
		$restricted_term_ids = self::get_user_restricted_term_ids( $user_id );
		if ( !empty( $restricted_term_ids ) && count( $restricted_term_ids) > 0) {
			$restricted_term_ids= "'" . implode( "','", array_map( 'esc_sql', $restricted_term_ids) ) . "'";
			$where .= " AND {$wpdb->comments}.comment_post_ID NOT IN ";
			$where .= " ( ";
			$where .= sprintf(
				"SELECT object_id FROM $wpdb->term_relationships " .
				"LEFT JOIN $wpdb->term_taxonomy ON {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->term_taxonomy}.term_taxonomy_id ".
				"WHERE term_id IN (%s) ",
				$restricted_term_ids
			);
			$where .= " ) ";
		}
		return $where;
	}

	/**
	 * Filter pages by their terms' access restrictions. Although pages don't
	 * have any terms related by default, this should be included if there
	 * are custom taxonomies related to pages.
	 *
	 * @param array $pages
	 * @return array
	 */
	public static function get_pages( $pages ) {

		if ( !apply_filters( 'groups_restrict_categories_get_pages_apply', true, $pages ) ) {
			return $pages;
		}

		$result = array();
		$user_id = get_current_user_id();
		foreach ( $pages as $page ) {
			if ( self::user_can_read( $page->ID, $user_id ) ) {
				$result[] = $page;
			}
		}
		return $result;
	}

	/**
	 * Filter posts by their terms' access restrictions.
	 *
	 * @param array $posts list of posts
	 * @param WP_Query $query
	 * @return array
	 */
	public static function the_posts( $posts, $query ) {

		if ( !apply_filters( 'groups_restrict_categories_the_posts_apply', true, $posts, $query ) ) {
			return $pages;
		}

		$result = array();
		$user_id = get_current_user_id();
		foreach ( $posts as $post ) {
			if ( self::user_can_read( $post->ID, $user_id ) ) {
				$result[] = $post;
			}
		}
		return $result;
	}

	/**
	 * Filter menu items by access capability.
	 *
	 * @todo admin section: this won't inhibit the items being offered to be added, although when they're added they won't show up in the menu
	 *
	 * @param array $items
	 * @param mixed $menu
	 * @param array $args
	 */
	public static function wp_get_nav_menu_items( $items = null, $menu = null, $args = null ) {

		if ( !apply_filters( 'groups_restrict_categories_wp_get_nav_menu_items_apply', true, $items, $menu, $args ) ) {
			return $items;
		}

		$result = array();
		if ( apply_filters( 'groups_restrict_categories_term_access_wp_get_nav_menu_items_apply', true, $items, $menu, $args ) ) {
			$user_id = get_current_user_id();
			foreach ( $items as $item ) {
				if ( self::controls_term( $item->object_id ) ) {
					if ( self::user_can_read_term( $user_id, $item->object_id ) ) {
						$result[] = $item;
					}
				} else {
					$result[] = $item;
				}
			}
		} else {
			$result = $items;
		}
		return $result;
	}

	/**
	 * Filter the excerpt by the post's related terms and their access 
	 * restrictions.
	 *
	 * @param string $output
	 * @return string the original output if access is granted, otherwise ''
	 */
	public static function get_the_excerpt( $output ) {

		global $post;

		if ( !apply_filters( 'groups_restrict_categories_get_the_excerpt_apply', true, $output ) ) {
			return $output;
		}

		$result = '';
		// only try to restrict if we have the ID
		if ( isset( $post->ID ) ) {
			if ( self::user_can_read( $post->ID ) ) {
				$result = $output;
			}
		} else {
			$result = $output;
		}
		return $result;
	}

	/**
	 * Filters the content by its related terms and their access restrictions.
	 *
	 * @param string $output
	 * @return string the original output if access is granted, otherwise ''
	 */
	public static function the_content( $output ) {

		global $post;

		if ( !apply_filters( 'groups_restrict_categories_the_conent_apply', true, $output ) ) {
			return $output;
		}

		$result = '';
		// only try to restrict if we have the ID
		if ( isset( $post->ID ) ) {
			if ( self::user_can_read( $post->ID ) ) {
				$result = $output;
			}
		} else {
			$result = $output;
		}
		return $result;
	}

	/**
	 * Returns true if all related terms allow access to the user. A single
	 * related term that restricts access will result in false to be returned.
	 *
	 * @param int $post_id
	 * @param int $user_id
	 * @return boolean
	 */
	public static function user_can_read( $post_id, $user_id = null ) {
		$result = true;
		if ( $user_id === null ) {
			$user_id = get_current_user_id();
		}

		$cached = class_exists( 'Groups_Cache' ) ? Groups_Cache::get( self::USER_CAN_READ . '_' . $post_id . '_' . $user_id, self::CACHE_GROUP ) : null;
		if ( $cached !== null ) {
			$result = $cached->value;
			unset( $cached );
		} else {
			if ( function_exists( '_groups_admin_override' ) && _groups_admin_override() ||
				current_user_can( GROUPS_ADMINISTER_GROUPS )
			) {
				$result = true;
			} else {
				$taxonomies = self::get_controlled_taxonomies();

				// Check the terms related to the post for the current user. Our filter must be disabled
				// so we can obtain the related terms even though they are not accessible for the current
				// user. Also we can't use get_the_terms() as it calls get_object_term_cache() which will
				// not include restricted terms for the current user when it has been called previously (
				// which is very likely), so we use wp_get_object_terms() instead:
				remove_filter( 'list_terms_exclusions', array( __CLASS__, 'list_terms_exclusions' ), 10 );
				$terms = wp_get_object_terms( $post_id, $taxonomies );
				add_filter( 'list_terms_exclusions', array( __CLASS__, 'list_terms_exclusions' ), 10, 3 );

				if ( is_array( $terms ) ) {
					foreach( $terms as $term ) {
						if ( !self::user_can_read_term( $user_id, $term->term_id ) ) {
							$result = false;
							break;
						}
					}
				}

			}
			$result = apply_filters( 'groups_restrict_categories_user_can_read_post', $result, $post_id, $user_id );
			if ( class_exists( 'Groups_Cache' ) ) {
				Groups_Cache::set( self::USER_CAN_READ . '_' . $post_id . '_' . $user_id, $result, self::CACHE_GROUP );
			}
		}
		return $result;
	}

	/**
	 * Controls permission to edit or delete posts based on the access
	 * restrictions of its related terms.
	 *
	 * If this were not handled, a user could access for example the post edit
	 * screen and modify it even though the post were restricted by term.
	 *
	 * @param array $caps
	 * @param string $cap
	 * @param int $user_id
	 * @param array $args
	 * @return array
	 */
	public static function map_meta_cap( $caps, $cap, $user_id, $args ) {
		if ( isset( $args[0] ) ) {
			if ( strpos( $cap, 'edit_' ) === 0 || strpos( $cap, 'delete_' ) === 0 ) {
				if ( $post_type = get_post_type( $args[0] ) ) {

					$edit_post_type   = 'edit_' . $post_type;
					$delete_post_type = 'delete_' . $post_type;
					if ( $post_type_object = get_post_type_object( $post_type ) ) {
						if ( !isset( $post_type_object->capabilities ) ) {
							$post_type_object->capabilities = array();
						}
						$caps_object = get_post_type_capabilities( $post_type_object );
						if ( isset( $caps_object->edit_post ) ) {
							$edit_post_type = $caps_object->edit_post;
						}
						if ( isset( $caps_object->delete_post ) ) {
							$delete_post_type = $caps_object->delete_post;
						}
					}

					if ( $cap === $edit_post_type || $cap === $delete_post_type ) {
						$post_id = null;
						if ( is_numeric( $args[0] ) ) {
							$post_id = $args[0];
						} else if ( $args[0] instanceof WP_Post ) {
							$post_id = $post->ID;
						}
						if ( $post_id ) {
							if ( !self::user_can_read( $post_id, $user_id ) ) {
								$caps[] = 'do_not_allow';
							}
						}
					}
				}
			}
		}
		return $caps;
	}

	/**
	 * WooCommerce purchasable filter.
	 *
	 * @param int $product_id
	 * @return product ID or null if not allowed
	 */
	public static function woocommerce_is_purchasable( $purchasable, $product ) {
		if ( method_exists( $product, 'get_id' ) ) {
			if ( $purchasable && !self::user_can_read( $product->get_id() ) ) {
				$purchasable = false;
			}
		} else {
			if ( $purchasable && !self::user_can_read( $product->id ) ) {
				$purchasable = false;
			}
		}
		return $purchasable;
	}

	/**
	 * Returns taxonomy objects handled by this extension.
	 * The groups_restrict_categories_get_taxonomies_args filter can be used
	 * to modify the query which restricts the taxonomies that are handled
	 * to those which fulfill public and show_ui are true.
	 *
	 * @param string $output 'objects' or 'names'
	 * @return array of object or string
	 */
	public static function get_taxonomies( $output = 'objects' ) {
		return get_taxonomies(
			apply_filters(
				'groups_restrict_categories_get_taxonomies_args',
				array(
					'public' => true,
					'show_ui' => true
				)
			),
			$output
		);
	}

	/**
	 * Returns true if the term is of a taxonomy that has
	 * access restrictions enabled.
	 *
	 * @param int $term_id
	 * @return boolean
	 */
	public static function controls_term( $term_id ) {

		global $wpdb;

		$cached = class_exists( 'Groups_Cache' ) ? Groups_Cache::get( self::CONTROLS_TERM . intval( $term_id ), self::CACHE_GROUP ) : null;
		if ( $cached !== null ) {
			$result = $cached->value;
			unset( $cached );
		} else {
			$taxonomy = $wpdb->get_var( $wpdb->prepare(
				"SELECT taxonomy FROM $wpdb->term_taxonomy WHERE term_id = %d",
				intval( $term_id )
			) );
			$result = self::controls_taxonomy( $taxonomy );
			$result = apply_filters( 'groups_restrict_categories_controls_term', $result, $term_id, $taxonomy );
			if ( class_exists( 'Groups_Cache' ) ) {
				Groups_Cache::set( self::CONTROLS_TERM . intval( $term_id ), $result );
			}
		}
		return $result;
	}

	/**
	 * Returns true if access restrictions are enabled for the taxonomy.
	 *
	 * @param string $taxonomy taxonomy name
	 * @return boolean
	 */
	public static function controls_taxonomy( $taxonomy ) {
		return apply_filters(
			'groups_restrict_categories_controls_taxonomy',
			in_array( $taxonomy, self::get_controlled_taxonomies() ),
			$taxonomy
		);
	}

	/**
	 * Returns an array of taxonomy names for which access restrictions are
	 * enabled.
	 *
	 * @return array of string
	 */
	public static function get_controlled_taxonomies() {
		$options = self::get_options();
		return isset( $options['taxonomies'] ) ? $options['taxonomies'] : self::get_taxonomies( 'names' );
	}

	/**
	 * Determines taxonomies for which access restrictions are enabled.
	 *  
	 * @param array $taxonomies taxonomy names
	 */
	public static function set_controlled_taxonomies( $taxonomies ) {
		if ( is_array( $taxonomies ) ) {
			$_taxonomies = array();
			foreach( $taxonomies as $taxonomy ) {
				if ( taxonomy_exists( $taxonomy ) ) {
					$_taxonomies[] = $taxonomy;
				}
			}
			$options = self::get_options();
			$options['taxonomies'] = $_taxonomies;
			self::set_options( $options );
		}
	}

	/**
	 * Get plugin options.
	 * @return array
	 */
	public static function get_options() {
		$data = get_option( self::OPTIONS, null );
		if ( $data === null ) {
			if ( add_option( self::OPTIONS, array(), '', 'no' ) ) {
				$data = get_option( self::OPTIONS, null );
			}
		}
		return $data;
	}

	/**
	 * Set plugin options.
	 * @param array $data
	 */
	public static function set_options( $data ) {
		$current_data = get_option( self::OPTIONS, null );
		if ( $current_data === null ) {
			add_option( self::OPTIONS, $data, '', 'no' );
		} else {
			update_option( self::OPTIONS, $data );
		}
	}

	/**
	 * Set the groups that grant read access for the term.
	 *
	 * If no group ids are provided, removes all groups related to the term for read access.
	 *
	 * @param int $term_id
	 * @param array $group_ids
	 */
	public static function set_term_read_groups( $term_id, $group_ids ) {
		if ( function_exists( 'get_term_meta' ) && function_exists( 'add_term_meta' ) && function_exists( 'delete_term_meta' ) ) {
			if ( !empty( $term_id ) ) {
				if ( !empty( $group_ids ) ) {
					if ( is_array( $group_ids ) ) {
						$group_ids = array_map( array( 'Groups_Utility', 'id' ), $group_ids );
						// remove groups not present
						$groups_read = array_map( array( 'Groups_Utility', 'id' ), get_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, false ) );
						foreach( $groups_read as $group_id ) {
							if ( !in_array( $group_id, $group_ids ) ) {
								delete_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, $group_id );
							}
						}
						// add groups
						foreach( $group_ids as $group_id ) {
							if ( Groups_Group::read( $group_id ) ) {
								if ( !in_array( $group_id, get_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, false ) ) ) {
									add_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, $group_id );
								}
							}
						}
					}
				} else {
					delete_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ );
				}
			}
		} else {
			_doing_it_wrong( __METHOD__, __( 'Groups Restrict Categories requires WordPress 4.4.0 or later.'), GRC_PLUGIN_VERSION );
		}
	}

	/**
	 * Returns an array of group IDs that grant read access to the term.
	 *
	 * @param int $term_id
	 * @return array
	 */
	public static function get_term_read_groups( $term_id ) {
		$result = null;
		if ( function_exists( 'get_term_meta' ) ) {
			$result = get_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, false );
		} else {
			_doing_it_wrong( __METHOD__, __( 'Groups Restrict Categories requires WordPress 4.4.0 or later.'), GRC_PLUGIN_VERSION );
		}
		return $result;
	}

	/**
	 * Removes the group IDs from restricting read access to the term.
	 *
	 * @param int $term_id
	 * @param array $group_ids
	 */
	public static function delete_term_read_groups( $term_id, $group_ids ) {
		if ( function_exists( 'delete_term_meta' ) ) {
			if ( !empty( $term_id ) ) {
				if ( !empty( $group_ids ) ) {
					if ( is_array( $group_ids ) ) {
						$group_ids = array_map( array( 'Groups_Utility', 'id' ), $group_ids );
						// remove groups
						foreach( $groups_ids as $group_id ) {
							delete_term_meta( $term_id, self::TERMMETA_PREFIX . self::READ, $group_id );
						}
					}
				}
			}
		} else {
			_doing_it_wrong( __METHOD__, __( 'Groups Restrict Categories requires WordPress 4.4.0 or later.'), GRC_PLUGIN_VERSION );
		}
	}

	/**
	 * Set the read capabilities for the term.
	 *
	 * @deprecated
	 *
	 * @param int $term_id
	 * @param array $capabilities
	 */
	public static function set_term_read_capabilities( $term_id, $capabilities ) {
		if ( class_exists( 'Groups_Restrict_Categories_Legacy' ) && method_exists( 'Groups_Restrict_Categories_Legacy', 'set_term_read_capabilities' ) ) {
			Groups_Restrict_Categories_Legacy::set_term_read_capabilities( $term_id, $capabilities );
		}
	}

	/**
	 * Returns an array of read capabilities for the term.
	 *
	 * @deprecated
	 *
	 * @param int $term_id
	 * @return array of string with read capabilities for the term, null if the term does not exist
	 */
	public static function get_term_read_capabilities( $term_id ) {
		$result = null;
		if ( class_exists( 'Groups_Restrict_Categories_Legacy' ) && method_exists( 'Groups_Restrict_Categories_Legacy', 'get_term_read_capabilities' ) ) {
			$result = Groups_Restrict_Categories_Legacy::get_term_read_capabilities( $term_id );
		}
		return $result;
	}

	/**
	 * Delete the read capabilities for the term.
	 *
	 * @deprecated
	 *
	 * @param int $term_id
	 */
	public static function delete_term_read_capabilities( $term_id ) {
		if ( class_exists( 'Groups_Restrict_Categories_Legacy' ) && method_exists( 'Groups_Restrict_Categories_Legacy', 'delete_term_read_capabilities' ) ) {
			Groups_Restrict_Categories_Legacy::delete_term_read_capabilities( $term_id );
		}
	}
}
Groups_Restrict_Categories::init();
