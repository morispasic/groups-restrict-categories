 =============================================================================
 Groups Restrict Categories WordPress plugin
 =============================================================================
 Copyright (c) 2014-2019 "kento" Karim Rahimpur www.itthinx.com
 =============================================================================

                              LICENSE RESTRICTIONS

            This plugin is provided subject to the license granted.
               Unauthorized use and distribution is prohibited.
                      See COPYRIGHT.txt and LICENSE.txt.

 =============================================================================

  You MUST be granted a license by the copyright holder for those parts that
  are not provided under the GPLv3 license.

  If you have not been granted a license DO NOT USE this plugin until you have
  BEEN GRANTED A LICENSE.

  Use of this plugin without a granted license constitutes an act of COPYRIGHT
  INFRINGEMENT and LICENSE VIOLATION and may result in legal action taken
  against the offending party.

  Being granted a license is GOOD because you will get support and contribute
  to the development of useful free and premium themes and plugins that you
  will be able to enjoy.

  Thank you!

  Visit www.itthinx.com for more information.

 =============================================================================

 * Groups

   http://www.itthinx.com/plugins/groups

 * Groups Restrict Categories

   http://www.itthinx.com/shop/groups-restrict-categories

 =============================================================================


 =============================================================================

    Table of Contents

    1) Documentation

 =============================================================================

1) Documentation
==============================================================================

     Please refer to the documentation pages for up-to-date instructions.

         http://docs.itthinx.com/document/groups-restrict-categories
